export const createCell = (x, y, isBomb = false) => ({
  position: { x: Number(x), y: Number(y) },
  isBomb,
  isFlagged: false,
  bombNeighbors: 0,
  isCovered: true
})
