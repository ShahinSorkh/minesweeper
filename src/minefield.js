import { createCell } from './cell.js'

export const createMinefield = (rows, cols, bombs) => ({
  totalCells: rows * cols,
  field: initField(rows, cols, bombs),
  gameOver: false,
  get flaggedBombCells () {
    return this.field.flat()
      .filter(c => c.isFlagged && c.isBomb)
  },
  get uncoveredCells () {
    return this.field.flat()
      .filter(c => !c.isCovered && !c.isFlagged)
  },
  get coveredCells () {
    return this.field.flat()
      .filter(c => c.isCovered && !c.isFlagged)
  },
  hitBomb: false,
  findNeighbors (cell) {
    return findNeighbors(this.field, cell.position.x, cell.position.y)
  },
  get state () {
    if (this.hitBomb) {
      console.log('you lost!')
      return { done: true, win: false }
    }

    if (this.flaggedBombCells.length === bombs || this.coveredCells.every(c => c.isBomb)) {
      console.log('you won!')
      return { done: true, win: true }
    }
    return { done: false }
  },
  async uncoverCell (cell) {
    if (!cell.isCovered || cell.isFlagged) {
      return
    }
    if (cell.isBomb) {
      this.hitBomb = true
      cell.isCovered = false
      return
    }

    cell.isCovered = false

    if (cell.bombNeighbors !== 0) {
      return
    }

    const visitedNeighbors = new Set()
    const q = [cell]
    while (q.length) {
      const cell = q.pop()
      visitedNeighbors.add(cell)
      const neighbors = this.findNeighbors(cell)
      for (const neighbor of neighbors) {
        if (!neighbor.isBomb) {
          if (neighbor.isCovered) {
            neighbor.isCovered = false
          }
          q.push(
            ...this.findNeighbors(cell)
              .filter(c => !visitedNeighbors.has(c))
              .filter(c => c.bombNeighbors === 0)
              .filter(c => !c.isFlagged)
          )
        }
      }
    }
  },
  async uncoverNeighbors (cell) {
    const flaggedNeighbors = this.findNeighbors(cell)
      .filter(c => c.isFlagged)
    if (cell.bombNeighbors <= flaggedNeighbors.length) {
      const neighbors = this.findNeighbors(cell)
      for (const neighbor of neighbors) {
        await this.uncoverCell(neighbor)
        if (this.state.done) return
      }
    }
  },
  async toggleFlag (cell) {
    if (!cell.isCovered && !cell.isFlagged) {
      return
    }

    cell.isFlagged = !cell.isFlagged
  },
  async uncoverEverything () {
    if (this.gameOver) return
    this.gameOver = true
    console.log('here 1')
    this.coveredCells.forEach(c => { c.isCovered = false })
  }
})

const findNeighbors = (field, x, y) => {
  const rows = field.length
  const cols = field[0].length
  const neighbors = []
  for (let di = -1; di <= 1; di++) {
    const i = x + di
    if (i < 0 || i >= cols) continue // out of field
    for (let dj = -1; dj <= 1; dj++) {
      if (dj === 0 && di === 0) continue // skip self cell
      const j = y + dj
      if (j < 0 || j >= rows) continue // out of field
      neighbors.push(field[j][i])
    }
  }
  return neighbors
}

const initField = (rows, cols, bombs) => {
  const field = Array(rows)
    .fill(null)
    .map((_) => Array(cols).fill(null))
  const placedBombs = new Set()
  while (placedBombs.size < bombs) {
    const pos = [
      Math.floor(Math.random() * rows),
      Math.floor(Math.random() * cols)
    ]
    if (placedBombs.has(pos)) {
      continue
    }
    placedBombs.add(pos)
    const [y, x] = pos
    field[y][x] = createCell(x, y, true)
  }
  for (const i in field) {
    for (const j in field[i]) {
      if (!field[i][j]) {
        field[i][j] = createCell(j, i)
      }
    }
  }
  for (const i in field) {
    for (const j in field[i]) {
      field[i][j].bombNeighbors = findNeighbors(field, Number(j), Number(i))
        .filter(cell => cell.isBomb).length
    }
  }
  return field
}
